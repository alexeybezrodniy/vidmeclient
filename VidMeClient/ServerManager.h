//
//  ServerManager.h
//  VidMeClient
//
//  Created by Олексій on 17.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerManager : NSObject

+ (ServerManager*) sharedManager;

- (void) getVideosWithOffset:(NSInteger) offset
                  andRequest:(NSString*) request
                       limit:(NSInteger) limit
                   omSuccess:(void(^)(NSArray* videosArray)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


@end
