//
//  VideoCell.m
//  VidMeClient
//
//  Created by Олексій on 18.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "VideoCell.h"
#import "UIImageView+AFNetworking.h"


@interface VideoCell()

@end

@implementation VideoCell

- (void) setThumbnailFromVideo:(VideoModel*) video{
    self.video = video;

    NSURLRequest* imgRequest = [NSURLRequest requestWithURL:video.thumbnailUrl];
    
    __weak VideoCell *weakCell = self;
    
    self.thumbnail.image = nil;
    
    [self.thumbnail setImageWithURLRequest:imgRequest placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        weakCell.thumbnail.image = image;
        [weakCell layoutSubviews];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
    
    self.videoTitle.text = video.title;
    self.likesCount.text = [NSString stringWithFormat:@"%ld", (long)video.likes];
}

@end
