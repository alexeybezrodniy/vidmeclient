//
//  ServerManager.m
//  VidMeClient
//
//  Created by Олексій on 17.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "ServerManager.h"
#import "AFNetworking.h"


@interface ServerManager ()


@end

@implementation ServerManager



+ (ServerManager*) sharedManager{
    static ServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    
    return manager;
}




- (void) getVideosWithOffset:(NSInteger) offset andRequest:(NSString*) request
                       limit:(NSInteger) limit
                   omSuccess:(void(^)(NSArray* videosArray)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @(offset), @"offset",
                            @(limit), @"limit", nil];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                        [manager GET:request//@"https://api.vid.me/videos/featured"
                          parameters:params
                            progress:nil
                             success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
                                 NSArray* videos = [responseObject objectForKey:@"videos"];
                                 if(success){
                                     success(videos);
                                 }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if(failure){
            failure(error, 0);
        }
    }];
    
}

@end
