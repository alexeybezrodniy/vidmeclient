//
//  FeaturedTableViewController.h
//  VidMeClient
//
//  Created by Олексій on 17.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyData.h"

@interface FeaturedTableViewController : UITableViewController



@property (strong, nonatomic) NSString* requestUrl;


- (IBAction)refresh:(UIRefreshControl *)sender;
- (void) setRequestUrl;
@end
