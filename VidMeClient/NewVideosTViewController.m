//
//  NewVideosTViewController.m
//  VidMeClient
//
//  Created by Олексій on 21.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "NewVideosTViewController.h"


@interface NewVideosTViewController ()

@end

@implementation NewVideosTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"New";
    
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setRequestUrl{
    MyData *data = [[MyData alloc] init];
    self.requestUrl = [data.URLs objectForKey:@"new"];
    //self.requestUrl = @"https://api.vid.me/videos/new";
}

@end
