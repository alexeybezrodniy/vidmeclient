//
//  FeedTableViewController.m
//  VidMeClient
//
//  Created by Олексій on 22.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "FeedTableViewController.h"
#import "LoginViewController.h"

@interface FeedTableViewController ()

@end

@implementation FeedTableViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    // Do any additional setup after loading the view.
    self.navigationController.toolbarHidden = NO;
    self.navigationController.navigationBar.hidden = YES;
    
    UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:0 target:self action:@selector(actionLogout)];
    UIBarButtonItem *flexibleSpaceBarButton = [[UIBarButtonItem alloc]
                                               initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                               target:nil
                                               action:nil];
    [self setToolbarItems:[NSArray arrayWithObjects:flexibleSpaceBarButton, item, flexibleSpaceBarButton, nil]];
    self.navigationItem.title = @"Feed";
}

- (void) actionLogout{
    NSLog(@"Logout");
    [self performSegueWithIdentifier:@"loginViewSegue" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    LoginViewController* vc = segue.destinationViewController;
    vc.token = nil;
    vc.username.text = @"";
    vc.password.text = @"";
    [vc saveToken];
    
}

- (void) setRequestUrl{
    self.requestUrl = self.requestWithToken;
}


@end
