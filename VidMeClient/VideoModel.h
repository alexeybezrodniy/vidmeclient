//
//  VideoModel.h
//  VidMeClient
//
//  Created by Олексій on 18.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoModel : NSObject

@property (assign, nonatomic) NSInteger likes;
@property (assign, nonatomic) NSInteger height;
@property (assign, nonatomic) NSInteger width;

@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSURL* videoUrl;
@property (strong, nonatomic) NSURL* thumbnailUrl;

- (id)initWithDictionary:(NSDictionary*) dictionary;

@end
