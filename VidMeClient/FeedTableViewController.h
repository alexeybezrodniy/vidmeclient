//
//  FeedTableViewController.h
//  VidMeClient
//
//  Created by Олексій on 22.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "FeaturedTableViewController.h"

@interface FeedTableViewController : FeaturedTableViewController

@property (strong, nonatomic) NSString* requestWithToken;

@end
