//
//  URLs.m
//  VidMeClient
//
//  Created by Олексій on 23.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "MyData.h"

@implementation MyData

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.URLs = [NSDictionary dictionaryWithObjectsAndKeys:
                     @"https://api.vid.me/videos/featured", @"featured",
                     @"https://api.vid.me/videos/new", @"new",
                     @"https://api.vid.me/auth/create", @"create",
                     @"https://api.vid.me/videos/following?token=", @"token",
                     nil];
    }
    return self;
}

@end
