//
//  NewVideosTableViewController.m
//  VidMeClient
//
//  Created by Олексій on 20.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "NewVideosTableViewController.h"
#import "ServerManager.h"
#import "VideoCell.h"
#import "VideoModel.h"

@import AVFoundation;
@import AVKit;

@interface NewVideosTableViewController () <UITableViewDataSource, UITableViewDelegate, UITabBarDelegate>

@property (strong, nonatomic) NSMutableArray* videosNew;
@property (strong, nonatomic) VideoCell* currentVideo;
@property (assign, nonatomic) BOOL isPlaing;

@end

@implementation NewVideosTableViewController

static NSInteger videosInRequest = 1;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.videosNew = [NSMutableArray array];
    
    [self getNewVideosFromServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dictionariesToVideoModel:(NSArray*) dicts{
    VideoModel* video = nil;
    for(NSDictionary* dict in dicts){
        video = [[VideoModel alloc] initWithDictionary:dict];
        [self.videosNew addObject:video];
    }
}

#pragma mark - API

- (void) getNewVideosFromServer{
    [[ServerManager sharedManager]
     getVideosWithOffset:[self.videosNew count]
     andRequest:@"https://api.vid.me/videos/new"
     limit:videosInRequest
     omSuccess:^(NSArray *videosArray) {
         [self dictionariesToVideoModel:videosArray];
         NSMutableArray *newPaths = [NSMutableArray array];
         for(int i = (int)[self.videosNew count] - (int)[videosArray count]; i < [self.videosNew count]; i++){
             [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         [self.tableView beginUpdates];
         [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationTop];
         [self.tableView endUpdates];
     }
     onFailure:^(NSError *error, NSInteger statusCode) {
         NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
     }];
}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.videosNew count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"Cell1";
    VideoModel* video = [self.videosNew objectAtIndex:indexPath.row];
    VideoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell){
        cell = [[VideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [cell createPlayerWithURL:video.videoUrl andHeight:video.height andWidth:video.width];
    cell.likesCount.text = [NSString stringWithFormat:@"%ld", (long)video.likes];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }else{
        
        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    }
    
    VideoCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if(!self.isPlaing){
        self.currentVideo = cell;
        [self.currentVideo playVideo];
        self.isPlaing = YES;
    }else{
        [self.currentVideo pauseVideo];
        self.isPlaing = NO;
        if(![cell isEqual:self.currentVideo]){
            self.currentVideo = cell;
            [self.currentVideo playVideo];
            self.isPlaing = YES;
        }
        
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
