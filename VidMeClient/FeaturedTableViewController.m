//
//  FeaturedTableViewController.m
//  VidMeClient
//
//  Created by Олексій on 17.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "FeaturedTableViewController.h"
#import "ServerManager.h"
#import "VideoCell.h"
#import "VideoModel.h"
#import "VideoViewController.h"


@interface FeaturedTableViewController () <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) NSMutableArray* videos;

@end

@implementation FeaturedTableViewController

static NSInteger videosInRequest = 10;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.videos = [NSMutableArray array];
    [self setRequestUrl];
    [self getVideosFromServer];
    
    self.navigationItem.title = @"Featured Videos";
    
    self.navigationController.navigationBar.hidden = YES;
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    [self.tabBarController.tabBar setHidden:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dictionariesToVideoModel:(NSArray*) dicts{
    VideoModel* video = nil;
    for(NSDictionary* dict in dicts){
        video = [[VideoModel alloc] initWithDictionary:dict];
        if(video.height != nil || video.width != nil){
            [self.videos addObject:video];
        }
        
    }
}


- (void) setRequestUrl{
    MyData *data = [[MyData alloc] init];
    self.requestUrl = [data.URLs objectForKey:@"featured"];
    
}

/*
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

}*/

#pragma mark pool-to-refresh

- (IBAction)refresh:(UIRefreshControl *)sender {
    self.videos = [NSMutableArray array];
    [self getVideosFromServer];
    [self.tableView reloadData];
    [sender endRefreshing];
}

#pragma mark - API

- (void) getVideosFromServer{
    [[ServerManager sharedManager]
                    getVideosWithOffset:[self.videos count]
                             andRequest:self.requestUrl
                                  limit:videosInRequest
                              omSuccess:^(NSArray *videosArray) {
                                  [self dictionariesToVideoModel:videosArray];
                                  NSMutableArray *newPaths = [NSMutableArray array];
                                  for(int i = (int)[self.videos count] - (int)[videosArray count]; i < [self.videos count]; i++){
                                      [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                                  }
                                  
                                  [self.tableView beginUpdates];
                                  [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationTop];
                                  [self.tableView endUpdates];
                              }
                              onFailure:^(NSError *error, NSInteger statusCode) {
                                  NSLog(@"error(inet?) = %@, code = %ld", [error localizedDescription], (long)statusCode);
                                  [self noIntertenConnection:self];
                                  
                              }];
}

#pragma mark - Error message

- (void)noIntertenConnection:(id)sender
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Error!"
                                                                        message:@"No internet connection. \nPlease, try again later"
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDestructive
                                                        handler:^(UIAlertAction *action) {
                                                           // NSLog(@"Dismiss button tapped!");
                                                        }];
    [controller addAction:alertAction];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  
    return [self.videos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"Cell";
    VideoModel* video = [self.videos objectAtIndex:indexPath.row];
    VideoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell){
        cell = [[VideoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    [cell setThumbnailFromVideo:video];

    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        if ([indexPath isEqual:[NSIndexPath indexPathForRow:[self tableView:self.tableView numberOfRowsInSection:0]-1 inSection:0]])
        {
            [self getVideosFromServer];
        }
    
}



#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    VideoViewController* vc = [[VideoViewController alloc] initWithVideoModel:cell.video];
    
    [self.navigationController pushViewController:vc animated:NO];

}


@end
