//
//  URLs.h
//  VidMeClient
//
//  Created by Олексій on 23.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyData : NSObject

@property (strong, nonatomic) NSDictionary* URLs;

@end
