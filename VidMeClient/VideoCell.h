//
//  VideoCell.h
//  VidMeClient
//
//  Created by Олексій on 18.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"


@interface VideoCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView* thumbnail;
@property (strong, nonatomic) IBOutlet UILabel* likesCount;
@property (strong, nonatomic) IBOutlet UILabel* videoTitle;
@property (strong, nonnull) VideoModel* video;


- (void) setThumbnailFromVideo:(VideoModel*) video;

@end
