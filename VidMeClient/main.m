//
//  main.m
//  VidMeClient
//
//  Created by Олексій on 17.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
