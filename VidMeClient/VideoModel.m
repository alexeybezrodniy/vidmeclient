//
//  VideoModel.m
//  VidMeClient
//
//  Created by Олексій on 18.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

- (id)initWithDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if (self) {
        @try{
            self.height =       [[dictionary objectForKey:@"height"] intValue];
            self.width =        [[dictionary objectForKey:@"width"] intValue];
            self.likes =        [[dictionary objectForKey:@"likes_count"] intValue];
            
        }@catch(NSException * e){
            NSLog(@"Exception: %@", e);
        }@finally{
            self.title =        [dictionary objectForKey:@"title"];
            self.videoUrl =     [NSURL URLWithString:[dictionary objectForKey:@"complete_url"]];
            self.thumbnailUrl = [NSURL URLWithString:[dictionary objectForKey:@"thumbnail_url"]];
        }
    }
    return self;
}

@end
