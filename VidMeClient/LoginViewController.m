//
//  LoginViewController.m
//  VidMeClient
//
//  Created by Олексій on 21.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "LoginViewController.h"
#import "AFNetworking.h"
#import "FeedTableViewController.h"
#import "MyData.h"

@interface LoginViewController () <NSURLSessionDataDelegate, UITextFieldDelegate>


@end

static NSString* kSettingsToken = @"token";

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadToken];
    if(self.token){
        
        [self performSegueWithIdentifier:@"feedVideosSegue" sender:self];
    }
    [self.navigationItem setHidesBackButton:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
   // self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    MyData* data = [[MyData alloc] init];
    NSString* requestToken = [data.URLs objectForKey:@"token"];
    FeedTableViewController* vc = segue.destinationViewController;
    vc.requestWithToken = [NSString stringWithFormat:@"%@%@", requestToken, self.token];
}

#pragma mark - Save and Load
- (void) saveToken{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.token forKey:kSettingsToken];
   // NSLog(@"token - %@", self.token);
    [userDefaults synchronize];
}

- (void) loadToken{
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults objectForKey:kSettingsToken]){
       self.token = [userDefaults objectForKey:kSettingsToken];
        NSLog(@"Token loaded - %@", self.token);
    }
    
  //  NSLog(@"Token loaded - %@", self.token);
}

#pragma mark - NSURLSessionDataDelegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    NSLog(@"### handler 1");
    
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received String %@",str);
    
    NSError *jsonError;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    NSString *status =[NSString stringWithFormat:@"%@", [json objectForKey:@"status"]];
    NSString* error =[NSString stringWithFormat:@"%@", [json objectForKey:@"error"]];
    if([status isEqualToString:@"0"]){
        NSLog(@"Status %@", status);
        [self wrongUsernameOrPWD:self andError:error];
      }
    self.token = [[json objectForKey:@"auth"] objectForKey:@"token"];
    [self saveToken];
}
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error == nil)
    {
        NSLog(@"Download is Succesfull");
        if(self.token != nil){
            [self performSegueWithIdentifier:@"feedVideosSegue" sender:self];
        }
        
    }
    else
        NSLog(@"Error %@",[error userInfo]);
}

- (BOOL)checkAuth{
    
    return YES;
}

#pragma mark - Error messages

- (void)tooShortInput:(id)sender
{
    NSString* fieldName;
    if([sender isEqual:self.username]){
        fieldName = @"Username";
    }
    if([sender isEqual:self.password]){
        fieldName = @"Password";
    }
    NSString* errorMessage = [NSString stringWithFormat:@"%@ too short!", fieldName];
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                        message:errorMessage
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDestructive
                                                        handler:^(UIAlertAction *action) {
                                                        }];
    [controller addAction:alertAction];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)wrongUsernameOrPWD:(id)sender andError:(NSString*) error
{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                        message:error
                                                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction *action) {
                                                        }];
    [controller addAction:alertAction];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:self.username]){
        [self.password becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
        [self actionLogin:self.login];
    }
    return YES;
}

#pragma mark - Actions

- (IBAction)actionLogin:(id)sender {
    if([self.username.text length] < 3){
        [self tooShortInput:self.username];
    }
    else if([self.password.text length] < 3){
        [self tooShortInput:self.password];
    }
    else{
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
        MyData* data = [[MyData alloc] init];
        NSString* URLString = [data.URLs objectForKey:@"create"];
        NSURL * url = [NSURL URLWithString:URLString];
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        NSString * params = [NSString stringWithFormat:@"password=%@&username=%@", self.password.text, self.username.text];
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:urlRequest];
        [dataTask resume];
    }
    
}
@end
