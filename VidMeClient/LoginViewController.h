//
//  LoginViewController.h
//  VidMeClient
//
//  Created by Олексій on 21.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField* username;
@property (strong, nonatomic) IBOutlet UITextField* password;
@property (strong, nonatomic) IBOutlet UIButton* login;
@property (strong, nonatomic) NSString* token;

- (IBAction)actionLogin:(id)sender;

- (void) saveToken;


@end
