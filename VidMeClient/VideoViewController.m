//
//  VideoViewController.m
//  VidMeClient
//
//  Created by Олексій on 21.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import "VideoViewController.h"
@import AVFoundation;
@import AVKit;


@interface VideoViewController ()

@property (strong, nonatomic) AVPlayer* videoPlayer;
@property (strong, nonatomic) UIView* videoView;
@property (strong, nonatomic) AVPlayerItem* videoPlayerItem;
@property (strong, nonatomic) AVPlayerLayer* playerLayer;

@end

@implementation VideoViewController

- (instancetype)initWithVideoModel:(VideoModel*)video
{
    self = [super init];
    if (self) {
        self.video = video;
        self.videoView = [self makeVideoView:video];
        [self.view addSubview:self.videoView];
        AVAsset *asset = [AVAsset assetWithURL:video.videoUrl];
        self.videoPlayerItem = [AVPlayerItem playerItemWithAsset:asset];
        self.videoPlayer = [[AVPlayer alloc] initWithPlayerItem:self.videoPlayerItem];
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.videoPlayer];
        [self.playerLayer setFrame:self.videoView.bounds];
        [self.videoView.layer addSublayer:self.playerLayer];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.videoPlayer play];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView*)makeVideoView:(VideoModel*) video{
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    CGFloat coef = 1.f;
    NSInteger maxWidth = window.frame.size.width;
    if(video.width > maxWidth){
        coef = video.width / maxWidth;
    }
    NSInteger originY = ((window.frame.size.height - video.height) / 2);
    NSInteger originX = 0;
    if(video.width < maxWidth){
        originX = ((window.frame.size.width - video.width) / 2);
    }
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(originX, originY, video.width/coef, video.height/coef)];
    return view;
}

@end
