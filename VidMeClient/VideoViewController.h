//
//  VideoViewController.h
//  VidMeClient
//
//  Created by Олексій on 21.10.16.
//  Copyright © 2016 Олексій. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"

@interface VideoViewController : UIViewController

@property (strong, nonatomic) VideoModel* video;

- (instancetype)initWithVideoModel:(VideoModel*)video;

@end
